package com.motumweb.platform.servicesubscriptions.dtos;

import java.util.Date;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AgreementDTO {
	
	@NotNull()
	private String clientService;
	
	@NotNull()
	private String agreementType;
	
	@NotNull()
	private String billingPlan;
	
	@NotNull()
	private String agreementStatus;
	
	@NotNull
	private Integer clientId;
	
	@NotNull()
	private Integer userId;
	
	@NotNull()
	private Date startDate;
	
	private Date endDate;
	
	private Date cancellationDate;
	
	private Date procesingDate;

}
