package com.motumweb.platform.servicesubscriptions.dtos;

import java.util.Date;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ServiceDTO {
	
	@NotNull()
	private Integer serviceId;
	
	@NotNull()
	private String service;
	
	@NotNull()
	private Integer agreementTypeId;
	
	@NotNull()
	private String agreementType;
	
	@NotNull()
	private Date startDate;
	
	@NotNull()
	private Date endDate;
	
	
}
