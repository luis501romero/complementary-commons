package com.motumweb.platform.servicesubscriptions.exceptions;

public class NotFoundException extends Exception {
	
	private String code;

	private static final long serialVersionUID = 1L;

	public NotFoundException(String code, String errorMessage) {
		super(errorMessage);	
		this.code = code;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}