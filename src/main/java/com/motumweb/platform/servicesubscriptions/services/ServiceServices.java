package com.motumweb.platform.servicesubscriptions.services;

import com.motumweb.platform.servicesubscriptions.dtos.ServiceDTO;
import com.motumweb.platform.servicesubscriptions.exceptions.NotFoundException;

public interface ServiceServices {
	
	public ServiceDTO getAgreementByServiceIdentifier(Integer clientId, Integer userId, Integer serviceId) throws NotFoundException;

}
