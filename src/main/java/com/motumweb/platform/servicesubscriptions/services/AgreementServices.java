package com.motumweb.platform.servicesubscriptions.services;

import com.motumweb.platform.servicesubscriptions.dtos.AgreementDTO;
import com.motumweb.platform.servicesubscriptions.exceptions.NotFoundException;

public interface AgreementServices {
	
	public void saveAgreement(Integer clientId, Integer userId, AgreementDTO agreement) throws NotFoundException;

}
